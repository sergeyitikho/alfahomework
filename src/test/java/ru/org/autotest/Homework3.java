package ru.org.autotest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import javax.xml.bind.Element;
import java.io.FileWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;

import java.io.*;

public class Homework3 {
    private WebDriver driver;
    private WebDriverWait wait;



    @Before
    public void start(){
        driver=new ChromeDriver();
        wait = new WebDriverWait(driver,10);
    }

    @Test
    public void Autotest() throws Exception {

        driver.get("https://yandex.ru/");
        String searchName=driver.getTitle();
        driver.findElement(By.xpath("//*[@id=\"text\"]")).sendKeys("Альфа-Банк");
        driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div[2]/div/div[2]/div/div[2]/div/form/div[2]/button")).click();
        driver.findElement(By.xpath("/html/body/div[3]/div[1]/div[2]/div[1]/div[1]/ul/li[2]/div/div[1]/div[1]/a/b")).click();
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1)); // переключение на вторую вкладу,0 - первая.
        wait.until(titleIs("Альфа-Банк - кредитные и дебетовые карты, кредиты наличными, автокредитование, ипотека и другие банковские услуги физическим и юридическим лицам – Альфа-Банк"));
        driver.findElement(By.xpath("//*[@id=\"alfa\"]/div/div[4]/div/div[2]/div[1]/div/a[2]")).click();
        wait.until(titleIs("Поиск работы, стажировка для студентов - вакансии Альфа-Банк, г. Москва"));
        driver.findElement(By.xpath("/html/body/div[1]/div/nav/nav/span[5]/a")).click();
        String about0 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]")).getText();
        String about1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]")).getText();



        Date dateNow = new Date();
        SimpleDateFormat myDate = new SimpleDateFormat("dd.MM.yyyy'-'hh:mm:ss");
        String fullDate=(myDate.format(dateNow));

        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        String browserName = cap.getBrowserName().toLowerCase();

        String fileName=fullDate+"-"+browserName+"-"+searchName;

        FileWriter nFile = new FileWriter(fileName,true);
        nFile.write(about0);
        nFile.write("\n\n");
        nFile.write(about1);
        nFile.close();
    }
    @After

    public void stop(){
        driver.quit();
        driver=null;
    }



}
