package ru.org.autotest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementSelectionStateToBe;
import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs ;
public class Homework2 {
    private WebDriver driver;
    private WebDriverWait wait;



    @Before
    public void start(){
        driver=new ChromeDriver();
        wait = new WebDriverWait(driver,10);
    }

    @Test

    public void AutoTest(){

        driver.get("https://yandex.ru/");

        driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div[2]/div/div[2]/div/div[1]/div/a[5]")).click(); // Переход на маркет
        wait.until(titleIs("Яндекс.Маркет — выбор и покупка товаров из проверенных интернет-магазинов")); //Ожидание заголовка маркета

        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/noindex/ul/li[1]/a")).click(); //Переход в электронику
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("body > div.main > div:nth-child(3) > div > div > div > h1"),"Электроника")); // Ожидание заголовка

        driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[1]/div/div[1]/div/a[1]")).click();//Переход в мобильные телефоны
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("body > div.main > div.n-page-search.i-bem.layout.layout_type_maya > div.headline.i-bem.headline_js_inited > div.headline__header.headline__header_no_recipes > h1"),"Мобильные телефоны")); // Ожидание заголовка

        driver.findElement(By.xpath("//*[@id=\"glpricefrom\"]")).sendKeys("40000"); //Вводим базовую стоимость
        driver.findElement(By.xpath("//*[@id=\"search-prepack\"]/div/div/div[2]/div/div[1]/div[4]/fieldset/ul/li[10]/div/a/label/div/span")).click();//Выбираем вендора.(Samsung)

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("body > div.main > div.n-page-search.i-bem.layout.layout_type_maya > div.headline.i-bem.headline_js_inited > div.headline__header.headline__header_no_recipes > h1"),"Мобильные телефоны Samsung")); //Ожидание срабатывания фильтра и загрузки списка
        String mob_title = driver.findElement(By.cssSelector("body > div.main > div:nth-child(5) > div.layout.layout_type_search.i-bem > div.layout__col.i-bem.layout__col_search-results_normal > div.n-filter-applied-results.metrika.b-zone.i-bem.n-filter-applied-results_js_inited.b-zone_js_inited > div > div.n-snippet-list.n-snippet-list_type_grid.snippet-list_size_3.metrika.b-zone.b-spy-init.i-bem.metrika_js_inited.snippet-list_js_inited.b-spy-init_js_inited.b-zone_js_inited > div:nth-child(1) > div.n-snippet-cell2__header > div.n-snippet-cell2__title > a")).getText(); //Берём название телефона и запихиваем в  первую переменную.

        driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[2]/div[1]/div[2]/div/div[1]/div[1]/a/img")).click();//Заходим на страницу телефона
        String mob_title2 = driver.findElement(By.cssSelector("body > div.main > div.n-product-summary.b-zone.i-bem.n-product-summary_js_inited.b-zone_js_inited > div.layout.layout_type_maya > div.n-product-summary__headline > div > div.n-product-title__text-container > div.n-product-title__text-container-top > div")).getText();//Присваиваем название во вторую переменную

        //сравниваем стринги
        if (mob_title.equals(mob_title2)) {
            System.out.println("Названия идентичны.("+ mob_title+" = "+mob_title2+").");
        }else{
            System.out.println("Названия отличаются.\n"+mob_title+"\n"+mob_title2+"\n");

        }
        //Наушники,переход из карточки телефона в электронику.

        driver.findElement(By.xpath("/html/body/div[1]/div[2]/noindex/ul/li[1]/a")).click();
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("body > div.main > div:nth-child(3) > div > div > div > h1"),"Электроника")); // Ожидание заголовка

        driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[1]/div/div[2]/div/a[1]")).click();
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("body > div.main > div.n-page-search.i-bem.layout.layout_type_maya > div.headline.i-bem.headline_js_inited > div.headline__header.headline__header_no_recipes > h1"),"Наушники и Bluetooth-гарнитуры"));

        driver.findElement(By.xpath("//*[@id=\"search-prepack\"]/div/div/div[2]/div/div[1]/div[4]/fieldset/ul/li[3]/div/a/label/div/span")).click();
        driver.findElement(By.xpath("//*[@id=\"glpricefrom\"]")).sendKeys("17000");
        driver.findElement(By.xpath("//*[@id=\"glpriceto\"]")).sendKeys("25000");
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("body > div.main > div.n-page-search.i-bem.layout.layout_type_maya > div.headline.i-bem.headline_js_inited > div.headline__header.headline__header_no_recipes > h1"),"Наушники и Bluetooth-гарнитуры Beats"));

        String ears_title = driver.findElement(By.cssSelector("body > div.main > div:nth-child(5) > div.layout.layout_type_search.i-bem > div.layout__col.i-bem.layout__col_search-results_normal > div.n-filter-applied-results.metrika.b-zone.i-bem.n-filter-applied-results_js_inited.b-zone_js_inited > div > div.n-snippet-list.n-snippet-list_type_grid.snippet-list_size_3.metrika.b-zone.b-spy-init.i-bem.metrika_js_inited.snippet-list_js_inited.b-spy-init_js_inited.b-zone_js_inited > div:nth-child(1) > div.n-snippet-cell2__header > div.n-snippet-cell2__title > a")).getText(); //Берём название первого телефона и запихиваем в переменную.

        driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[2]/div[1]/div[2]/div/div[1]/div[1]/a/img")).click();

        String ears_title2 = driver.findElement(By.cssSelector("body > div.main > div.n-product-summary.b-zone.i-bem.n-product-summary_js_inited.b-zone_js_inited > div.layout.layout_type_maya > div.n-product-summary__headline > div > div.n-product-title__text-container > div.n-product-title__text-container-top > div")).getText();

        if (mob_title.equals(mob_title2)) {
            System.out.println("Названия идентичны.("+ ears_title+" = "+ears_title2+").");
        }else{
            System.out.println("Названия отличаются.\n"+ears_title+"\n"+ears_title2+"\n");

        }
    }

    @After

    public void stop(){
        driver.quit();
        driver=null;
    }



}
