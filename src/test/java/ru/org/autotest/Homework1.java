package ru.org.autotest;
import java.io.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Homework1 {
    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new FileReader(new File("notes.txt")));

        List<Integer>list = new ArrayList<Integer>();
        String line;
        while((line = br.readLine())!= null){
            String [] r = line.split(",");

            for(int i = 0; i < r.length; i++){
                int val = Integer.parseInt(r[i]);
                list.add(val);
            }
        }br.close();
        System.out.println("Original :"+list);
        Collections.sort(list);
        System.out.println("Sorted   :"+list);;
        Collections.reverse(list);
        System.out.println("Reversed :"+list);
    }
}
